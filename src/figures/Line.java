package figures;

public class Line extends Shape {
    private double length;
    private double startX;
    private double endX;
    private double startY;
    private double endY;

    public Line(double length, double startX, double endX,
                double startY, double endY) {
        super((startX+endX)/2, (startY + endY)/2);
        this.length = length;
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
    }

    @Override
    public String toString() {
        return "Line{" +
                "length=" + length +
                ", startX=" + startX +
                ", endX=" + endX +
                ", startY=" + startY +
                ", endY=" + endY +
                '}';
    }
}
