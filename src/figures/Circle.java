package figures;

public class Circle extends SolidFigure {
    private double radius;

    public Circle(double centerX, double centerY, double radius) {
        super(centerX, centerY);
        setRadius(radius);
    }

    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Circle{ " + super.toString() +
                ", radius = " + radius +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Circle) {
            Circle circle = (Circle) obj;
            return this.getCenterX() == circle.getCenterX()
                    && this.getCenterY() == circle.getCenterY()
                    && this.radius == circle.radius;
        } else {
            return false;
        }
    }

    public void setRadius(double radius) {
        this.radius = radius;
        setArea(calculateArea());
        setPerimeter(calculatePerimeter());
    }

}
