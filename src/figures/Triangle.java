package figures;

public class Triangle extends SolidFigure {
    private Point point1;
    private Point point2;
    private Point point3;

    public Triangle(Point point1, Point point2, Point point3) {

        super((point1.getCenterX() + point2.getCenterX() + point3.getCenterX()) / 3,
                (point1.getCenterY() + point2.getCenterY() + point3.getCenterY()) / 3);
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }


    @Override
    public double calculateArea() {
        double halfPerimeter = calculatePerimeter() / 2;
        double area = Math.sqrt(halfPerimeter *
                (halfPerimeter - point1.calculateDistance(point2)) *
                (halfPerimeter - point1.calculateDistance(point3)) *
                (halfPerimeter - point2.calculateDistance(point3)));
        return area;
    }

    @Override
    public String toString() {
        return "Triangle{ " +
                "point1: " + point1 +
                ", point2: " + point2 +
                ", point3: " + point3 +
                '}';
    }

    @Override
    public double calculatePerimeter() {
        return point1.calculateDistance(point2)
                + point2.calculateDistance(point3)
                + point3.calculateDistance(point1);
    }
}
