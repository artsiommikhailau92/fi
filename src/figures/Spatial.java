package figures;

public interface Spatial {
    double calculateArea();
    double calculatePerimeter();
}
